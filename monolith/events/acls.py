from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json
#Any code in here should return Python dictionaries to be used by your view code.

def get_picture(search_term):
    try:
        url = f'https://api.pexels.com/v1/search?query={search_term}'
        headers = {"Authorization": PEXELS_API_KEY}
        r = requests.get(url, headers=headers)
        picture_details = json.loads(r.content)
        picture_url = picture_details['photos'][0]['url']
        return {"picture_url": picture_url}
    except:
        return {"picture_url": "null"}




def get_weather(city, state):
    try:
        url = f'http://api.openweathermap.org/geo/1.0/direct?q={city},{state},{'US'}&limit={1}&appid={OPEN_WEATHER_API_KEY}'
        r = requests.get(url)
        if r.status_code == 200:
            lon_lat = json.loads(r.content)
            lon = lon_lat[0]['lon']
            lat = lon_lat[0]['lat']
        else:
            return {"weather": None}
        url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&units=imperial&appid={OPEN_WEATHER_API_KEY}"
        r = requests.get(url)
        if r.status_code == 200:
            weather_details = json.loads(r.content)
            weather = {
                "temp": weather_details['main']['temp'],
                "description": weather_details['weather'][0]['description'],
            }
        else:
            return {"weather": None}
        return weather
    except:
        return {"weather": None}
